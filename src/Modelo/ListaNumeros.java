/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Negocio.*;
import java.util.Arrays;

/**
 * Nuestra primera estructura : Vector--> ListaNumeros
 *a
 * @author steeven
 */
public class ListaNumeros implements Comparable{

    private float[] numeros;

    public ListaNumeros() {
    }

    public ListaNumeros(int cant) {
        if (cant <= 0) {
            throw new RuntimeException("No se puede crear el vector:"+cant);
        }

        this.numeros = new float[cant];

    }

    /**
     * Adiciona un número en la posición i
     *
     * @param i indice donde se va a ingresar el dato
     * @param numeroNuevo el dato a ingresar
     */
    public void adicionar(int i, float numeroNuevo) {
        this.validar(i);
        this.numeros[i] = numeroNuevo;
    }

    public float getElemento(int i) {

        this.validar(i);
        return this.numeros[i];
    }

    private void validar(int i) {
        if (i < 0 || i >= this.numeros.length) {
            throw new RuntimeException("Índice fuera de rango:" + i);
        }
    }

    /*
        ES REDUNDANTE
    **/
    public void actualizar(int i, float numeroNuevo) {
        this.adicionar(i, numeroNuevo);
    }

    public float[] getNumeros() {
        return numeros;
    }

    public void setNumeros(float[] numeros) {
        this.numeros = numeros;
    }

    @Override
    public String toString() {
        String msg = "  ";
        /*
            Cuando sólo son recorridos, usamos el foreach
            for(T elemento:coleccion)
                hacer algo con elemento;
         */

        for (float dato : this.numeros) {
            msg += dato + "   ";
        }
        return msg;
    }

    public int length() {
        return this.numeros.length;
    }

    
    
    
    
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Arrays.hashCode(this.numeros);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ListaNumeros other = (ListaNumeros) obj;

        if (this.numeros.length != other.numeros.length) {
            return false;
        }

        for (int i = 0; i < this.numeros.length; i++) {
            if (this.numeros[i] != other.numeros[i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Una lista es mayor a otra si y solo si:
     * 1. Tienen el mismo tamaño
     * 2. Todos los elementos de la lista1 son mayores a los de la lista 2
     * en orden ( suponga que la lista 1 y 2 se encuentra ordenada)
     * @param o
     * @return 
     */
    @Override
    public int compareTo(Object o) {
        
        if(this == o){
            
            return 0;
        }
        
        if(o == null){
            throw new RuntimeException("No se puede comparar porque el segundo objeto es null");
            
        }
        
        if(getClass() != o.getClass()){
            
            throw new RuntimeException("Objetos no son de la misma clase");
        }
        final ListaNumeros other = (ListaNumeros) o;
        
        int n = 0;
        
        if(this.numeros.length == other.numeros.length){
            for(int i = 0; i< this.numeros.length; i++ ){
                n = (int) (this.numeros[i] - other.numeros[i]);
            }
            
        }else{
            throw new RuntimeException("No se pueden comparar, tamaños diferentes");
        }
        return n;
    }

}
