/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.ListaNumeros;

/**
 * Modelamiento de una matriz usando el concepto de vector de vectores
 *
 * @author madarme
 */
public class Matriz_Numeros {

    private ListaNumeros[] filas;

    public Matriz_Numeros() {
    }

    public Matriz_Numeros(String cadena) {
        if (cadena.isEmpty()) {
            throw new RuntimeException("No se puede cargar la matriz, cadena vacía");
        }

        String filaDatos[] = cadena.split(";");

        this.filas = new ListaNumeros[filaDatos.length];
        for (int i = 0; i < filaDatos.length; i++) {
            String columnaDatos[] = filaDatos[i].split(",");
            ListaNumeros nuevaColumnas = new ListaNumeros(columnaDatos.length);
            this.pasarDatosColumna(nuevaColumnas, columnaDatos);
            this.filas[i] = nuevaColumnas;

        }
    }

    private void pasarDatosColumna(ListaNumeros col, String datos[]) {
      
        for (int j = 0; j < datos.length; j++) {
            col.adicionar(j, Float.parseFloat(datos[j]));
        }
        
        
    }

    /**
     * Puedo utilizarlo para matrices dispersa
     *
     * @param cantFilas
     */
    public Matriz_Numeros(int cantFilas) {
        if (cantFilas <= 0) {
            throw new RuntimeException("No se puede crear la matriz , sus cantidad de filas debe ser mayor a 0");
        }
        this.filas = new ListaNumeros[cantFilas];
    }

    /**
     * Creación de matrices cuadradas o bien rectangulares
     *
     * @param cantFilas
     * @param cantColumnas
     */
    public Matriz_Numeros(int cantFilas, int cantColumnas) {
        if (cantFilas <= 0 || cantColumnas <= 0) {
            throw new RuntimeException("No se puede crear la matriz , sus cantidad de filas o columnas debe ser mayor a 0");
        }
        this.filas = new ListaNumeros[cantFilas];
        //Creando columnas:
        for (int i = 0; i < cantFilas; i++) {
            this.filas[i] = new ListaNumeros(cantColumnas);
        }

    }

    public ListaNumeros[] getFilas() {
        return filas;
    }

    public void setFilas(ListaNumeros[] filas) {
        this.filas = filas;
    }

    private void validar(int i) {
        if (i < 0 || i >= this.filas.length) {
            throw new RuntimeException("Índice fuera de rango para una fila:" + i);
        }
    }

    public void adicionarVector(int i, ListaNumeros listaNueva) {
        this.validar(i);
        this.filas[i] = listaNueva;
    }

    /**
     * Esté mètodo funciona SI Y SOLO SI LA MATRIZ ESTÁ CREADA CON FILAS Y
     * COLUMNAS
     *
     * @param i índice de la fila
     * @param j índice de la columna
     * @param nuevoDato dato a ingresar en i,j
     */
    public void setElemento(int i, int j, float nuevoDato) {
        this.filas[i].adicionar(j, nuevoDato);
    }

    @Override
    public String toString() {
        String msg = "   ";

        for (ListaNumeros myLista : this.filas) {
            msg += myLista.toString() + "\n  ";
        }
        return msg;
    }

    public int length_filas() {
        return this.filas.length;
    }

    /**
     *
     * 🅼🅴🆃🅾🅳🅾🆂 🅳🅴 🅻🅰 🅰🅲🆃🅸🆅🅸🅳🅰🅳
     */
    public boolean validezSumaYResta(Matriz_Numeros matriz) {

        boolean valido = true;

        if (this.filas.length != matriz.length_filas()) {
            valido = false;
        } else {
            for (int i = 0; i < this.filas.length; i++) {
                if (this.filas[i].length() != matriz.getFilas()[i].length()) {
                    valido = false;
                }
            }
        }

        return valido;
    }

    public Matriz_Numeros getSuma(Matriz_Numeros matriz2) {

        if (!validezSumaYResta(matriz2)) {
            throw new RuntimeException("No se puede realizar la suma: el número de filas y columas de las 2 matrices no son iguales");
        }

        Matriz_Numeros matrizSuma = new Matriz_Numeros(this.filas.length, this.filas[0].length());

        for (int i = 0; i < this.filas.length; i++) {
            for (int j = 0; j < this.filas[i].length(); j++) {
                float resultadoSuma = this.filas[i].getElemento(j) + matriz2.getFilas()[i].getElemento(j);
                matrizSuma.setElemento(i, j, resultadoSuma);
            }
        }
        //
        return matrizSuma;
    }

    public Matriz_Numeros getResta(Matriz_Numeros matriz2) {

        if (!validezSumaYResta(matriz2)) {
            throw new RuntimeException("No se puede realizar la resta: el número de filas y columas de las 2 matrices no son iguales");
        }

        Matriz_Numeros matrizResta = new Matriz_Numeros(this.filas.length, this.filas[0].length());

        for (int i = 0; i < this.filas.length; i++) {
            for (int j = 0; j < this.filas[i].length(); j++) {
                float resultadoResta = this.filas[i].getElemento(j) - matriz2.getFilas()[i].getElemento(j);
                matrizResta.setElemento(i, j, resultadoResta);
            }
        }
        //
        return matrizResta;
    }

    public Matriz_Numeros getMultiplicacion(Matriz_Numeros matriz2) {

        if (!validacionMultiplicacion(matriz2)) {
            throw new RuntimeException("No se puede realizar la múltiplicación: el número de columnas "
                    + "de la primera matriz es diferentes al número de filas de la segunda matriz");
        }

        Matriz_Numeros matrizMultiplicacion = new Matriz_Numeros(this.getFilas().length, matriz2.getFilas()[0].length());

        float resultado = 0;
        for (int i = 0; i < this.filas.length; i++) {
            for (int j = 0; j < matriz2.getFilas()[0].length(); j++) {
                for (int k = 0; k < this.getFilas()[0].length(); k++) {
                    resultado += this.filas[i].getElemento(k) * matriz2.getFilas()[k].getElemento(j);
                    matrizMultiplicacion.setElemento(i, j, resultado);
                }
                resultado = 0;
            }
        }

        return matrizMultiplicacion;
    }

    public void getTranspuesta() {

        if (!validacionTranspuesta()) {
            throw new RuntimeException("No se puede realizar la Transposición: Su matriz"
                    + "es dispersa");
        }
        
        Matriz_Numeros aux = new Matriz_Numeros(this.filas[0].length(), this.filas.length);

        for (int i = 0; i < this.filas.length; i++) {
            for (int j = 0; j < this.filas[i].length(); j++) {
                aux.getFilas()[j].adicionar(i, this.filas[i].getElemento(j));
            }
        }

        this.filas = aux.filas;
    }

    public boolean validacionMultiplicacion(Matriz_Numeros matriz) {
        boolean valido = true;

        if (this.filas[0].length() != matriz.length_filas()) {
            valido = false;
        } else {
            for (int i = 1; i < this.filas.length; i++) {
                if (this.filas[i - 1].length() != this.filas[i].length()) {
                    valido = false;
                }
            }

            for (int i = 1; i < matriz.length_filas(); i++) {
                if (matriz.getFilas()[i - 1].length() != matriz.getFilas()[i].length()) {
                    valido = false;
                }
            }

        }

        return valido;
    }

    public boolean validacionTranspuesta() {
        
        boolean valido = true;

        for (int i = 0; i < this.filas.length - 1; i++) {
            if (this.filas[i].length() != this.filas[i + 1].length()) {
                valido = false;
            }
        }
        return valido;
    }

}